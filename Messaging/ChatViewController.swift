//
//  ViewController.swift
//  Messaging
//
//  Created by Sajjad Khazraei on 3/14/20.
//  Copyright © 2020 Bornak. All rights reserved.
//

import UIKit
import CoreData

class ChatViewController: UICollectionViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var messages: [DateComponents:[MessageModel]]? = {
        var allMessages = [MessageModel]()
        if let managedMessages = try? LocalDataManager.shared.retrieveMessages() {
            for managedMessage in managedMessages {
                let message = CoreDataMapper.managedObjectToModel(managedObject: managedMessage)
                allMessages.append(message)
            }
        }
        
        let groupDic = Dictionary(grouping: allMessages) { (message) -> DateComponents in
            let date = Calendar.current.dateComponents([.day, .year, .month], from: (Date(timeIntervalSince1970: message.timestamp ?? 0)))
            return date
        }
        
        print(groupDic.keys.count)
        return groupDic
    }()
    var fromUser: UserModel?
    var toUser: UserModel?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        messages = [MessageModel]()
//        for _ in 1...3 {
//            messages?.append(MessageModel.getOutMessageMock())
//        }
//        for _ in 1...2 {
//            messages?.append(MessageModel.getInMessageMock())
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        collectionView.alwaysBounceVertical = true
        collectionView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
        collectionView.keyboardDismissMode = .interactive
        
        retrieveUsers()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"), style: .plain, target: nil, action: nil)
        
        navigationController?.navigationBar.tintColor = UIColor.white
        
        let borderView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 0.5))
        borderView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.6)
        self.navigationController?.navigationBar.shadowImage = UIImage(view: borderView)
        
        avatarImageView.image = UIImage(named: "avatar")
        avatarImageView.layer.cornerRadius = 18
        avatarImageView.clipsToBounds = true
        
        titleLabel.text = toUser?.name
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "arrow-left"), style: .plain, target: nil, action: nil)
    }
    
    lazy var messageInputView: UIView? = {
        guard let inputView = Bundle.main.loadNibNamed("MessageInputView", owner: nil, options: nil)?.first as? MessageInputView else {
            return nil
        }
        inputView.delegate = self
        return inputView
    }()
    
    override var inputAccessoryView: UIView? {
        get {
            return messageInputView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }

}

extension ChatViewController: UICollectionViewDelegateFlowLayout {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return messages?.count ?? 0;
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let sortedKey = messages?.keys.sorted(by: {
            Calendar.current.date(from: $0) ?? Date.distantFuture <
                Calendar.current.date(from: $1) ?? Date.distantFuture
        })
        let sectionMessages = messages?[sortedKey![section]]
        
        return sectionMessages?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let sortedKey = messages?.keys.sorted(by: {
            Calendar.current.date(from: $0) ?? Date.distantFuture <
                Calendar.current.date(from: $1) ?? Date.distantFuture
        })
        let sectionMessages = messages?[sortedKey![indexPath.section]]
        let message = sectionMessages?[indexPath.item]
        
        let cellId = message?.voice != nil ? "AudioMessageCell" : "MessageCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        
        if message?.voice != nil {
            (cell as! AudioMessageCell).message = message
            (cell as! AudioMessageCell).fillContents()
        } else {
            (cell as! MessageCell).message = message
            (cell as! MessageCell).fillContents()
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let sortedKey = messages?.keys.sorted(by: {
                Calendar.current.date(from: $0) ?? Date.distantFuture <
                    Calendar.current.date(from: $1) ?? Date.distantFuture
            })
            let sectionMessages = messages?[sortedKey![indexPath.section]]
            let message = sectionMessages?[indexPath.item]
            
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CollectionViewHeader", for: indexPath) as! CollectionViewHeader
            
            header.fillContent(title: dayDifference(from: message?.timestamp ?? 0))
            
            return header
        }
        
        return UICollectionReusableView()
    }
    
    func dayDifference(from interval : TimeInterval) -> String
    {
        let calendar = Calendar.current
        let date = Date(timeIntervalSince1970: interval)
        if calendar.isDateInYesterday(date) { return "Yesterday" }
        else if calendar.isDateInToday(date) { return "Today" }
        else if calendar.isDateInTomorrow(date) { return "Tomorrow" }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 { return "\(-day) days ago" }
            else { return "In \(day) days" }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 44)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: UIScreen.main.bounds.size.width, height: 50)
        
        let sortedKey = messages?.keys.sorted(by: {
            Calendar.current.date(from: $0) ?? Date.distantFuture <
                Calendar.current.date(from: $1) ?? Date.distantFuture
        })
        let sectionMessages = messages?[sortedKey![indexPath.section]]
        let message = sectionMessages?[indexPath.item]
        
        if let text = message?.text {
            let height = MessageCell.calculateHeigh(text: text)
            size = CGSize(width: UIScreen.main.bounds.size.width, height: height + 8)
        }
        
        return size
    }

}

extension ChatViewController: InputViewDelegate {
    func inputStateChanged(_ state: InputState) {
        //
    }
    
    func messageSent(_ message: MessageModel) {
        saveMessage(message: message)
    }
    
    func saveMessage(message: MessageModel) {
        let date = Calendar.current.dateComponents([.day, .year, .month], from: (Date(timeIntervalSince1970: message.timestamp ?? 0)))
        
        messages?[date]?.append(message)
        collectionView.reloadData()
        
        do {
            if let managedOC = CoreDataStore.managedObjectContext {
                if let newMessage = NSEntityDescription.entity(forEntityName: "Message", in: managedOC) {
                    let managedMessage = Message(entity: newMessage, insertInto: managedOC)
                    
                    managedMessage.text = message.text
                    managedMessage.voice = message.voice
                    managedMessage.voiceDuration = message.voiceDuration ?? 0
                    managedMessage.timestamp = message.timestamp ?? 0
                    managedMessage.status = message.status?.rawValue
                    
                    managedMessage.fromId = fromUser?.id
                    managedMessage.toId = toUser?.id
                    
                    try managedOC.save()
                }
            }
        } catch {
            print(error)
        }
    }
}

extension ChatViewController {
    func retrieveUsers() {
        if let currentUserData = UserDefaults.standard.object(forKey: "user") as? Data, let currentUser = try? PropertyListDecoder().decode(UserModel.self, from: currentUserData)  {
            if let user = try? LocalDataManager.shared.retrieveUser(id: currentUser.id ?? "") {
                fromUser = CoreDataMapper.managedObjectToModel(managedObject: user)
            }
        }
        
        
        if let user = try? LocalDataManager.shared.retrieveUser(id: "lalala") {
            toUser = CoreDataMapper.managedObjectToModel(managedObject: user)
        }
    }
    
    func setMessages() {
        var allMessages = [MessageModel]()
        if let managedMessages = try? LocalDataManager.shared.retrieveMessages() {
            for managedMessage in managedMessages {
                let message = CoreDataMapper.managedObjectToModel(managedObject: managedMessage)
                allMessages.append(message)
            }
        }
        
        let groupDic = Dictionary(grouping: allMessages) { (message) -> DateComponents in
            let date = Calendar.current.dateComponents([.day, .year, .month], from: (Date(timeIntervalSince1970: message.timestamp ?? 0)))
            return date
        }
        
        messages = groupDic
    }
}

