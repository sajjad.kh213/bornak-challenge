//
//  Message.swift
//  Messaging
//
//  Created by Sajjad Khazraei on 3/15/20.
//  Copyright © 2020 Bornak. All rights reserved.
//

import UIKit

class MessageModel: NSObject {

    var text: String?
    var voice: String?
    var voiceDuration: Double?
    var from: UserModel?
    var to: UserModel?
    var timestamp: Double?
    var status: MessageStatus?
    
}

enum MessageStatus: String {
    case delivered  = "delivered"
    case read       = "read"
}
