//
//  User.swift
//  Messaging
//
//  Created by Sajjad Khazraei on 3/15/20.
//  Copyright © 2020 Bornak. All rights reserved.
//

import UIKit

class UserModel: Codable {

    var name: String?
    var id: String?
    var avatar: String?
    
    static func getMock() -> UserModel {
        let user = UserModel()
        user.name = "Sajjad"
        user.id = "sasasa"
        
        return user
    }
    
}
