//
//  AudioMessageCell.swift
//  Messaging
//
//  Created by Sajjad Khazraei on 3/15/20.
//  Copyright © 2020 Bornak. All rights reserved.
//

import UIKit

class AudioMessageCell: UICollectionViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var doubleTick: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var bubbleTrailingConstraintIB: NSLayoutConstraint!
    
    var bubbleleadingConstraint: NSLayoutConstraint!
    var bubbleTrailingConstraint: NSLayoutConstraint!
    var isPlaying = false
    var isPaused = false
    var audioDuration: Double = 0
    var voiceURL: URL?
    
    
    var message: MessageModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupCell()
    }
    
    func setupCell() {
        bubbleView.layer.cornerRadius = 5;
        bubbleView.clipsToBounds = true;
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.width / 2
        avatarImageView.clipsToBounds = true
        avatarImageView.backgroundColor = UIColor.lightGray
        
        bubbleleadingConstraint = bubbleView.leftAnchor.constraint(equalTo: avatarImageView.rightAnchor, constant: 8)
        bubbleTrailingConstraint = bubbleTrailingConstraintIB != nil ? bubbleTrailingConstraintIB :  bubbleView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8)
        
        slider.setThumbImage(UIImage(named: "thumb"), for: .normal)
    }
    
    func fillContents() {
        if let currentUserData = UserDefaults.standard.object(forKey: "user") as? Data, let currentUser = try? PropertyListDecoder().decode(UserModel.self, from: currentUserData), message?.from?.id == currentUser.id {
            avatarImageView.isHidden = true
            bubbleTrailingConstraint.isActive = true
            bubbleleadingConstraint.isActive = false
            
            bubbleView.backgroundColor = UIColor(red: 79/255, green: 78/255, blue: 94/255, alpha: 1)
        } else {
            avatarImageView.isHidden = false
            bubbleTrailingConstraint.isActive = false
            bubbleleadingConstraint.isActive = true
            
            bubbleView.backgroundColor = UIColor(red: 32/255, green: 32/255, blue: 52/255, alpha: 1)
        }
        
        if let avatar = message?.to?.avatar, let url = URL(string: avatar) {
            avatarImageView.image = try? UIImage(withContentsOfUrl: url)
        }
        
        doubleTick.image = message?.status == .delivered ? UIImage(named: "double-tick-white") : UIImage(named: "double-tick-blue")
        
        audioDuration = message?.voiceDuration ?? 0
        timeLabel.text = getTimeString(time: Int(audioDuration.rounded()))
        
        if let name = message?.voice, let voice = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(name) {
            voiceURL = voice
        }
    }
    
    override func prepareForReuse() {
        message = nil
        isPlaying = false
        isPaused = false
        playButton.setImage(UIImage(named: "play"), for: .normal)
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        AudioPlayer.shared.skipAudioToPosition(position: Double(sender.value) * audioDuration)
    }
    
    @IBAction func playButtonTapped(_ sender: UIButton) {
        if isPlaying {
            AudioPlayer.shared.pause()
            self.isPaused = true
            self.isPlaying = false
        } else {
            if isPaused {
                AudioPlayer.shared.resume()
                isPlaying = true
                isPaused = false
            }
            else {
                AudioPlayer.shared.initPlayer(withURL: self.voiceURL!, delegate: self) {
                    AudioPlayer.shared.play() {
                        self.isPlaying = false
                        self.playButton.setImage(UIImage(named: "play"), for: .normal)
                        self.timeLabel.text = self.getTimeString(time: Int(self.audioDuration.rounded()))
                        self.slider.setValue(0.0, animated: false)
                    }
                    isPlaying = true
                }
            }
        }
        
        let buttonImage = isPlaying ? UIImage(named: "pause") : UIImage(named: "play")
        playButton.setImage(buttonImage, for: .normal)
    }
}

extension AudioMessageCell: AudioPlayerDelegate {
    func updateUI(current: Double, duration: Double) {
        audioDuration = duration
        
        timeLabel.text = getTimeString(time: Int(current.rounded()))
        
        if duration != 0 {
            slider.setValue(Float(current / duration), animated: true)
        }
    }
    
    func getTimeString(time: Int) -> String {
        let minutesString = String(format: "%01d", locale: Locale(identifier: "en_US"), Int(Float(time) / 60))
        let secondsString = String(format: "%02d", locale: Locale(identifier: "en_US"), time % 60)
        return "\(minutesString):\(secondsString)"
    }
}
