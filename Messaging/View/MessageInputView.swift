//
//  MessageInputView.swift
//  Messaging
//
//  Created by Sajjad Khazraei on 3/15/20.
//  Copyright © 2020 Bornak. All rights reserved.
//

import UIKit

protocol InputViewDelegate: class {
    func inputStateChanged(_ state: InputState)
    func messageSent(_ message: MessageModel)
}

enum InputState: String {
    case none = "none"
    case isTyping = "isTyping"
    case isRecording = "isRecording"
    case recorded = "recorded"
}

class MessageInputView: UIView {

    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var textViewContainer: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var textViewContainerTrailingConstraint: NSLayoutConstraint!
    
    var inputState: InputState = .none {
        didSet {
            if inputState == .isRecording {
                recordButton.isHidden = true
                cameraButton.isHidden = true
                plusButton.isHidden = true
                textViewContainer.isHidden = true
                sendButton.isHidden = false
            } else if inputState == .recorded {
                cancelButton.isHidden = true
            } else if inputState == .isTyping {
                cameraButton.isHidden = true
                recordButton.isHidden = true
                sendButton.isHidden = false
            } else {
                recordButton.isHidden = false
                cameraButton.isHidden = false
                plusButton.isHidden = false
                textViewContainer.isHidden = false
                sendButton.isHidden = true
            }
        }
    }
    weak var delegate: InputViewDelegate?
    
    private var isSwiped = false
    
    private var timer: Timer?
    private var duration: CGFloat = 0
    
    private var timerStackView: UIStackView!
    private var slideToCancelStackVIew: UIStackView!
    
    public var offset: CGFloat = 0
    private var sliderCenter: CGPoint!
    
    private lazy var touchDownAndUpGesture: iGesutreRecognizer! = {
        let gesture = iGesutreRecognizer(target: self, action: #selector(handleUpAndDown(_:)))
        gesture.gestureDelegate = self
        
        return gesture
    }()
    
    private lazy var moveGesture: UIPanGestureRecognizer! = {
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(touchMoved(_:)))
        
        return gesture
    }()
    
    private let cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(onSwipe), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isHidden = true
        
        return button
    }()
    
    private let arrow: UIImageView = {
        let arrowView = UIImageView()
        arrowView.image = UIImage(named: "arrow")
        arrowView.translatesAutoresizingMaskIntoConstraints = false
        arrowView.tintColor = .white
        return arrowView
    }()
    
    private let slideLabel: UILabel = {
        let slide = UILabel()
        slide.text = "Swipe to cancel"
        slide.translatesAutoresizingMaskIntoConstraints = false
        slide.font = slide.font.withSize(12)
        slide.textColor = .white
        return slide
    }()
    
    private var pinkMicrophoneImageView: UIImageView = {
        let michrophone = UIImageView()
        michrophone.image = UIImage(named: "microphone-pink")
        michrophone.translatesAutoresizingMaskIntoConstraints = false
        
        return michrophone
    }()
    
    private var timerLabel: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.font = label.font.withSize(12)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var coverView: UIView! = {
        let screenRect = UIScreen.main.bounds
        let view = UIView(frame: screenRect)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupView()
    }
    
    func setupView() {
        textViewContainer.layer.borderColor = UIColor.lightGray.cgColor
        textViewContainer.layer.borderWidth = 1
        textViewContainer.layer.cornerRadius = 22
        
        textView.text = "Say something!"
        textView.textColor = UIColor.lightGray
        
        
        pinkMicrophoneImageView.widthAnchor.constraint(equalToConstant: 25).isActive = true
        pinkMicrophoneImageView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        timerStackView = UIStackView(arrangedSubviews: [pinkMicrophoneImageView, timerLabel])
        timerStackView.translatesAutoresizingMaskIntoConstraints = false
        timerStackView.isHidden = true
        timerStackView.spacing = 8
        
        
        slideToCancelStackVIew = UIStackView(arrangedSubviews: [arrow, slideLabel])
        slideToCancelStackVIew.translatesAutoresizingMaskIntoConstraints = false
        slideToCancelStackVIew.isHidden = true
        
        
        addSubview(timerStackView)
        addSubview(slideToCancelStackVIew)
        addSubview(cancelButton)
        
        cancelButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        cancelButton.centerYAnchor.constraint(equalTo: timerStackView.centerYAnchor).isActive = true
        cancelButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        arrow.widthAnchor.constraint(equalToConstant: 10).isActive = true
        arrow.heightAnchor.constraint(equalToConstant: 10).isActive = true
        
        slideToCancelStackVIew.trailingAnchor.constraint(equalTo: self.sendButton.leadingAnchor, constant: -8).isActive = true
        slideToCancelStackVIew.centerYAnchor.constraint(equalTo: self.plusButton.centerYAnchor).isActive = true
        
        timerStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8).isActive = true
        timerStackView.centerYAnchor.constraint(equalTo: slideToCancelStackVIew.centerYAnchor).isActive = true
        
        recordButton.addGestureRecognizer(moveGesture)
        recordButton.addGestureRecognizer(touchDownAndUpGesture)
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        if inputState != .isTyping {
            onRecordingFinish()
        } else {
            handleSend()
        }
    }
    
    @objc func handleSend() {
        guard let currentUserData = UserDefaults.standard.object(forKey: "user") as? Data, let currentUser = try? PropertyListDecoder().decode(UserModel.self, from: currentUserData) else {
            return
        }
        
        let message = MessageModel()
        
        if inputState == .recorded {
            guard let voiceUrl = AudioRecorder.shared.finishRecording() else {
                return
            }
            message.voice = voiceUrl
            message.voiceDuration = Double(duration)
        } else {
            message.text = textView.text
        }
    
        message.status = .delivered
        message.from = currentUser
        message.to = UserModel.getMock()
        message.timestamp = Date().timeIntervalSince1970
        
//        inputState = .none
        textView.text = nil
        textViewDidChange(textView)
        textViewDidEndEditing(textView)
        delegate?.messageSent(message)
    }

}

extension MessageInputView {
    @objc private func updateDuration() {
        duration += 1
        timerLabel.text = duration.fromatSecondsFromTimer()
    }
    
    private func onRecordingStart() {
        guard AudioRecorder.shared.checkPermission() else {
            return
        }
        
        if let _ = superview {
            superview?.superview?.insertSubview(coverView, belowSubview: superview!)
        }
        
        resetTimer()
        
        isSwiped = false
        
        AudioRecorder.shared.startRecording()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateDuration), userInfo: nil, repeats: true)
        
        //reset all views to default
        slideToCancelStackVIew.transform = .identity
        recordButton.transform = .identity
        
        slideToCancelStackVIew.isHidden = false
        timerStackView.isHidden = false
        timerLabel.isHidden = false
        pinkMicrophoneImageView.isHidden = false
        
        inputState = .isRecording
        delegate?.inputStateChanged(inputState);
    }
    
    //this will be called when user swipes to the left and cancel the record
    @objc private func onSwipe() {
        isSwiped = true
        
        slideToCancelStackVIew.isHidden = true
        timerLabel.isHidden = true
        pinkMicrophoneImageView.isHidden = true
        cancelButton.isHidden = true
        
        resetTimer()
        
        coverView.removeFromSuperview()
        
        inputState = .none
        delegate?.inputStateChanged(inputState)
    }
    
    private func resetTimer() {
        timer?.invalidate()
        timerLabel.text = "00:00"
        duration = 0
    }
    
    private func onLockRecording() {
        slideToCancelStackVIew.isHidden = true
        cancelButton.isHidden = false
    }
    
    //this will be called when user lift his finger
    private func onRecordingFinish() {
        isSwiped = false
        
        coverView.removeFromSuperview()
        
        slideToCancelStackVIew.isHidden = true
        timerStackView.isHidden = true
        
        timerLabel.isHidden = true
        
        inputState = .recorded
        delegate?.inputStateChanged(.recorded)
        
        handleSend()
        
        resetTimer()
    }
    
    
    @objc private func handleUpAndDown(_ sender: UIGestureRecognizer) {
        switch sender.state {
        case .began:
            onRecordingStart()
            
        case .ended:
            onRecordingFinish()
            
        default:
            break
        }
    }
    
    //this will be called when user starts to move his finger
    @objc func touchMoved(_ sender: UIPanGestureRecognizer) {
        
        if isSwiped {
            return
        }
        
        let translation = sender.translation(in: recordButton)
        
        switch sender.state {
        case .began:
            sliderCenter = slideToCancelStackVIew.center
        case .changed:
            if translation.x < 0 {
                slideToCancelStackVIew.center = CGPoint(x: sliderCenter.x + translation.x, y: sliderCenter.y)                
                if slideToCancelStackVIew.frame.intersects(timerStackView.frame.offsetBy(dx: offset, dy: 0)) {
                    onSwipe()
                }
            }
        case .ended:
            let location = sender.location(in: self)
            if location.y < 0 {
                onLockRecording()
            } else {
                onRecordingFinish()
            }
            
        default:
            break
        }
        
    }
}

extension MessageInputView: GesutreDelegate {
    func onStart() {
        onRecordingStart()
    }
    
    func onEnd() {
        onRecordingFinish()
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        bringSubviewToFront(self)
    }
}

extension MessageInputView: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Say something!"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty {
            textViewContainerTrailingConstraint.constant = 94
            
            UIView.animate(withDuration: 0.2, animations: {
                self.layoutIfNeeded()
            }) { (finished: Bool) in
                self.inputState = .none
            }
        } else {
            inputState = .isTyping
            textViewContainerTrailingConstraint.constant = 68
            
            UIView.animate(withDuration: 0.2) {
                self.layoutIfNeeded()
            }
        }
    }
}

protocol GesutreDelegate {
    func onStart()
    func onEnd()
}


class iGesutreRecognizer: UIGestureRecognizer {
    var gestureDelegate: GesutreDelegate?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        gestureDelegate?.onStart()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        gestureDelegate?.onEnd()
    }
    
}

