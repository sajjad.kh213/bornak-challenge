//
//  CollectionViewHeader.swift
//  Messaging
//
//  Created by Sajjad Khazraei on 3/16/20.
//  Copyright © 2020 Bornak. All rights reserved.
//

import UIKit

class CollectionViewHeader: UICollectionReusableView {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func fillContent(title: String) {
        titleLabel.text = title
    }
    
}
