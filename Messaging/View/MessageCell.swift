//
//  MessageCell.swift
//  Messaging
//
//  Created by Sajjad Khazraei on 3/15/20.
//  Copyright © 2020 Bornak. All rights reserved.
//

import UIKit

class MessageCell: UICollectionViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var doubleTick: UIImageView!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var timestampTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var bubbleWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var textViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bubbleTrailingConstraintIB: NSLayoutConstraint!
    
    var bubbleleadingConstraint: NSLayoutConstraint!
    var bubbleTrailingConstraint: NSLayoutConstraint!
    
    var message: MessageModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupCell()
    }
    
    func setupCell() {
        bubbleView.layer.cornerRadius = 5;
        bubbleView.clipsToBounds = true;
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.width / 2
        avatarImageView.clipsToBounds = true
        avatarImageView.backgroundColor = UIColor.lightGray
        
        bubbleleadingConstraint = bubbleView.leftAnchor.constraint(equalTo: avatarImageView.rightAnchor, constant: 8)
        bubbleTrailingConstraint = bubbleTrailingConstraintIB != nil ? bubbleTrailingConstraintIB : bubbleView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8)
    }
    
    func fillContents() {
        if let currentUserData = UserDefaults.standard.object(forKey: "user") as? Data, let currentUser = try? PropertyListDecoder().decode(UserModel.self, from: currentUserData), message?.from?.id == currentUser.id {
            avatarImageView.isHidden = true
            bubbleTrailingConstraint.isActive = true
            bubbleleadingConstraint.isActive = false
            
            bubbleView.backgroundColor = UIColor(red: 79/255, green: 78/255, blue: 94/255, alpha: 1)
        } else {
            avatarImageView.isHidden = false
            bubbleTrailingConstraint.isActive = false
            bubbleleadingConstraint.isActive = true
            
            bubbleView.backgroundColor = UIColor(red: 32/255, green: 32/255, blue: 52/255, alpha: 1)
        }
        
//        if let avatar = message?.to?.avatar, let url = URL(string: avatar) {
//            avatarImageView.image = try? UIImage(withContentsOfUrl: url)
//        }
        avatarImageView.image = UIImage(named: "avatar")
        
        if let text = message?.text {
            textView.text = text
            let size = text.estimatedSize()
            let isLongText = size.width + 16 > (UIScreen.main.bounds.width * 0.8) - 83 || size.height > 20
            let width = size.width + 16 > (UIScreen.main.bounds.width * 0.8) - 83 ? UIScreen.main.bounds.width * 0.8 : size.width + 48
            bubbleWidthConstraint.constant = isLongText ? width : size.width + 113
            textViewBottomConstraint.constant = isLongText ? 23 : 8
            textViewTrailingConstraint.constant = isLongText ? 16 : 89
        }
        
        doubleTick.image = message?.status == .delivered ? UIImage(named: "double-tick-white") : UIImage(named: "double-tick-blue")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        if let timestamp = message?.timestamp {
            timestampLabel.text = formatter.string(from: Date(timeIntervalSince1970: timestamp))
        }
    }
    
    override func prepareForReuse() {
        message = nil
    }
    
    static func calculateHeigh(text: String) -> CGFloat {
        let size = text.estimatedSize()
        let isLongText = size.width + 16 > (UIScreen.main.bounds.width * 0.8) - 83 || size.height > 20
        
        return isLongText ? size.height + 38 : size.height + 16
    }
    
}
