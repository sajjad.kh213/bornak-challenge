//
//  ContentFactory.swift
//  Messaging
//
//  Created by Sajjad Khazraei on 3/16/20.
//  Copyright © 2020 Bornak. All rights reserved.
//

import UIKit

class ContentFactory: NSObject {
    static let shared = ContentFactory()
    override private init() {}
    
    var fromUser: UserModel?
    var toUser: UserModel?
    
    func mockInitialMessages() -> [MessageModel] {
        retrieveUsers()
        
        var messages = [MessageModel]()
        
        var message = MessageModel()
        message.text = "Coffee today?"
        message.status = .read
        message.timestamp = Date().addingTimeInterval(-86400).timeIntervalSince1970
        message.from = fromUser
        message.to = toUser
        messages.append(message)
        
        message = MessageModel()
        message.text = "Oh Yeah! Time?"
        message.status = .read
        message.timestamp = Date().addingTimeInterval(-86340).timeIntervalSince1970
        message.from = toUser
        message.to = fromUser
        messages.append(message)
        
        message = MessageModel()
        message.text = "5 pm?"
        message.status = .read
        message.timestamp = Date().addingTimeInterval(-86040).timeIntervalSince1970
        message.from = fromUser
        message.to = toUser
        messages.append(message)
        
        message = MessageModel()
        message.text = "OK"
        message.status = .read
        message.timestamp = Date().addingTimeInterval(-85980).timeIntervalSince1970
        message.from = toUser
        message.to = fromUser
        messages.append(message)
        
        message = MessageModel()
        message.text = "Lucks? :)"
        message.status = .read
        message.timestamp = Date().addingTimeInterval(-85970).timeIntervalSince1970
        message.from = toUser
        message.to = fromUser
        messages.append(message)
        
        message = MessageModel()
        message.text = "Oh yeah, for sure! see you late then :)"
        message.status = .read
        message.timestamp = Date().addingTimeInterval(-85380).timeIntervalSince1970
        message.from = fromUser
        message.to = toUser
        messages.append(message)
        
        message = MessageModel()
        message.text = "Wanna go out tonight?"
        message.status = .read
        message.timestamp = Date().timeIntervalSince1970
        message.from = toUser
        message.to = fromUser
        messages.append(message)
        
        return messages
    }
    
    func retrieveUsers() {
        if let currentUserData = UserDefaults.standard.object(forKey: "user") as? Data, let currentUser = try? PropertyListDecoder().decode(UserModel.self, from: currentUserData)  {
            if let user = try? LocalDataManager.shared.retrieveUser(id: currentUser.id ?? "") {
                fromUser = CoreDataMapper.managedObjectToModel(managedObject: user)
            }
        }
        
        
        if let user = try? LocalDataManager.shared.retrieveUser(id: "lalala") {
            toUser = CoreDataMapper.managedObjectToModel(managedObject: user)
        }
    }
}
