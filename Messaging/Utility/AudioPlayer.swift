//
//  AudioRecorder.swift
//  Messaging
//
//  Created by Sajjad Khazraei on 3/15/20.
//  Copyright © 2020 Bornak. All rights reserved.
//

import AVKit

protocol AudioPlayerDelegate {
    func updateUI(current: Double, duration: Double)
}

class AudioPlayer: NSObject {
    
    static let shared = AudioPlayer()
    private override init() {}
    
    private var delegate: AudioPlayerDelegate?
    private var player: AVAudioPlayer?
    private var fileURL: URL?
    
    private var completion: (() -> Void)?
    
    private var updateTimer: Timer?
    
    func initPlayer(withURL fileURL: URL, delegate: AudioPlayerDelegate? = nil, completion: () -> Void) {
        self.fileURL = fileURL
        self.delegate = delegate
        
        var fileSize : UInt64
        
        do {
            //return [FileAttributeKey : Any]
            let fullPath = ((self.fileURL?.path ?? "") as NSString).expandingTildeInPath
            let attr = try FileManager.default.attributesOfItem(atPath: fullPath)
            fileSize = attr[FileAttributeKey.size] as! UInt64
            
            //if you convert to NSDictionary, you can get file size old way as well.
            let dict = attr as NSDictionary
            fileSize = dict.fileSize()
            
            print(fileSize)
        } catch {
            print("Error: \(error)")
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: fileURL, fileTypeHint: AVFileType.m4a.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            player?.delegate = self
            player?.prepareToPlay()
            
            updateUI()
            completion()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func play(completion: @escaping () -> Void) {
        if let _ = self.completion {
            self.completion!()
        }
        self.completion = completion
        
        player?.play()
        
        updateTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(updateUI), userInfo: nil, repeats: true)
    }
    
    func resume() {
        player?.play()
        
        updateTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(updateUI), userInfo: nil, repeats: true)
    }
    
    func skipAudioToPosition(position: Double) {
        player?.currentTime = position
        if let isPlaying = player?.isPlaying, !isPlaying {
            player?.prepareToPlay()
        }
        updateUI()
    }
    
    func pause() {
        player?.pause()
        updateTimer?.invalidate()
    }
    
    func stop() {
        player?.stop()
        updateTimer?.invalidate()
        player?.currentTime = 0
        fileURL = nil
        updateUI()
    }
    
    @objc func updateUI() {
        delegate?.updateUI(current: player?.currentTime ?? 0.0, duration: player?.duration.rounded(.down) ?? 0)
    }
    
    func getCurrentTime() -> Double {
        return player?.currentTime ?? 0
    }
}

extension AudioPlayer: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        updateTimer?.invalidate()
        updateUI()
        if let _ = completion {
            completion!()
        }
    }
}
