//
//  AudioRecorder.swift
//  Messaging
//
//  Created by Sajjad Khazraei on 3/15/20.
//  Copyright © 2020 Bornak. All rights reserved.
//

import AVFoundation

class AudioRecorder: NSObject {
    static let shared = AudioRecorder()
    
    private var audioFileUrl: URL?
    private var audioFileName: String?
    
    lazy var recordingSession: AVAudioSession! = {
        let session = AVAudioSession.sharedInstance()
        return session
    }()
    var audioRecorder: AVAudioRecorder!
    
    func startRecording() {
        let identifier = UUID()
        audioFileName = identifier.uuidString + ".m4a"
        audioFileUrl = getDocumentsDirectory().appendingPathComponent(audioFileName!)
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.medium.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFileUrl!, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
        } catch {
            //
        }
    }
    
    func finishRecording() -> String? {
        guard let _ = audioRecorder else {
            return nil
        }
        
        audioRecorder.stop()
        audioRecorder = nil
        
        return audioFileName
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func checkPermission() -> Bool {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .granted:
            return true;
        case .denied:
            return false;
        case .undetermined:
            do {
                try recordingSession.setCategory(.playAndRecord, mode: .default)
                try recordingSession.setActive(true)
                recordingSession.requestRecordPermission() { allowed in
                    DispatchQueue.main.async {
                        if allowed {
                            //
                        } else {
                            // failed to record!
                        }
                    }
                }
            } catch {
                // failed to record!
            }
            return false;
        default:
            break
        }
        
        return false;
    }
}

extension AudioRecorder: AVAudioRecorderDelegate {
    
}
