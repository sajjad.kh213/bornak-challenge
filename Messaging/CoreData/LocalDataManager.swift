//
//  LocalDataManager.swift
//  Messaging
//
//  Created by Sajjad Khazraei on 3/15/20.
//  Copyright © 2020 Bornak. All rights reserved.
//

import CoreData

class LocalDataManager {
    static let shared = LocalDataManager()
    private init() {}
    
    func retrieveMessages() throws -> [Message]  {
        guard let managedOC = CoreDataStore.managedObjectContext else {
            return []
        }
        
        let request: NSFetchRequest<Message> = NSFetchRequest(entityName: String(describing: Message.self))
        let sort = NSSortDescriptor(key: "timestamp", ascending: true)
        request.sortDescriptors = [sort]
        
        return try managedOC.fetch(request)
    }
    
    func retrieveUser(id: String) throws -> User? {
        guard let managedOC = CoreDataStore.managedObjectContext else {
            return nil
        }
        
        let request: NSFetchRequest<User> = NSFetchRequest(entityName: String(describing: User.self))
        let predicate = NSPredicate(format: "id == %@", id)
        request.predicate = predicate
        
        let user = try managedOC.fetch(request)
        return user.first
    }
}
