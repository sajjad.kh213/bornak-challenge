//
//  CoreDataMapper.swift
//  Weno
//
//  Created by Sajjad Khazraei on 9/19/18.
//  Copyright © 2018 SOSHYA. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataMapper {
    
    static func managedObjectToModel(managedObject: User) -> UserModel {
        let user = UserModel()

        user.avatar = managedObject.avatar
        user.id = managedObject.id
        user.name = managedObject.name

        return user
    }
    
    static func managedObjectToModel(managedObject: Message) -> MessageModel {
        let message = MessageModel()
        
        message.text = managedObject.text
        message.voice = managedObject.voice
        message.voiceDuration = managedObject.voiceDuration
        message.timestamp = managedObject.timestamp
        message.status = (managedObject.status != nil) ? MessageStatus(rawValue: managedObject.status!) : nil
        
        if let fromId = managedObject.fromId, let fromUser = try? LocalDataManager.shared.retrieveUser(id: fromId) {
            message.from = managedObjectToModel(managedObject: fromUser)
        }
        
        if let toId = managedObject.fromId, let toUser = try? LocalDataManager.shared.retrieveUser(id: toId) {
            message.to = managedObjectToModel(managedObject: toUser)
        }
        
        return message
    }
    
}
